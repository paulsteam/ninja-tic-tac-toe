//
//  ViewController.swift
//  Tic Tac Toe
//
//  Created by Paul Pearson on 2/18/16.
//  Copyright © 2016 RPM Consulting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var naughtsTurn = false
    
    @IBOutlet var mainLabel: UILabel!
    @IBOutlet var whosnextButton: UIButton!
    @IBOutlet var button01: UIButton!
    @IBOutlet var button02: UIButton!
    @IBOutlet var button03: UIButton!
    @IBOutlet var button04: UIButton!
    @IBOutlet var button05: UIButton!
    @IBOutlet var button06: UIButton!
    @IBOutlet var button07: UIButton!
    @IBOutlet var button08: UIButton!
    @IBOutlet var button09: UIButton!
    
    @IBOutlet var playAgainButton: UIBarButtonItem!
    
    @IBOutlet var gameOverLabel: UILabel!
    
    var buttonsLocked = [false, false, false, false, false, false, false, false, false, false ]
    var theBoard = [
                    [0,0,0],
                    [0,0,0],
                    [0,0,0]
                   ]
    
    @IBAction func buttonPressed(sender: AnyObject) {
        setButton(sender as! UIButton)
    }
    
    @IBAction func button01pressed(sender: AnyObject) {
        //setButton(sender as! UIButton)
    }
    
    @IBAction func button02pressed(sender: AnyObject) {
        //setButton(sender as! UIButton)
    }
    
    @IBAction func button03pressed(sender: AnyObject) {
        //setButton(sender as! UIButton)
    }
    
    @IBAction func button04pressed(sender: AnyObject) {
        //setButton(sender as! UIButton)
    }
    
    @IBAction func button05pressed(sender: AnyObject) {
        //setButton(sender as! UIButton)
    }
    
    @IBAction func button06pressed(sender: AnyObject) {
        //setButton(sender as! UIButton)
    }
    
    @IBAction func button07pressed(sender: AnyObject) {
        //setButton(sender as! UIButton)
    }
    
    @IBAction func button08pressed(sender: AnyObject) {
        //setButton(sender as! UIButton)
    }
    
    @IBAction func button09pressed(sender: AnyObject) {
        //setButton(sender as! UIButton)
    }
    
    @IBAction func playAgainPressed(sender: AnyObject) {
        resetGame()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        resetGame()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func resetGame() {
        mainLabel.text = "Play!"
        // Set "whos next" button
        whosnextButton.setImage(UIImage(named: "x.png"), forState: .Normal)
        naughtsTurn = false
        for index in 0...9 {
            buttonsLocked[index] = false
        }
        for index in 0...8 {
            theBoard[index/3][index%3] = 0
        }
        // print(theBoard)

        // Remove all images for start of play....
        var buttonToClear : UIButton
        for var i = 1; i <= 9; ++i {
            buttonToClear = view.viewWithTag(i) as! UIButton
            buttonToClear.setImage(nil, forState: .Normal)
        }
        
        gameOverLabel.hidden = true
        gameOverLabel.center = CGPointMake(gameOverLabel.center.x - 500, gameOverLabel.center.y)
        
    }
    
    func lockAllButtons() {
        for var tag = 0; tag < buttonsLocked.count; ++tag {
            buttonsLocked[tag] = true
        }
    }

    func setButton(button: UIButton) {
        if !buttonsLocked[button.tag] {
            if naughtsTurn {
                button.setImage(UIImage(named: "o.png"), forState: .Normal)
                setBoard(button.tag,player: 1)
            } else {
                button.setImage(UIImage(named: "x.png"), forState: .Normal)
                setBoard(button.tag,player: 2)
            }
            buttonsLocked[button.tag] = true // lock the button
            switch getWinner() {
                case 1:
                    lockAllButtons()
                    // mainLabel.text = "O's WIN!!"
                    gameOverLabel.text = "O's Win!!"
                    gameOverLabel.hidden = false
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.gameOverLabel.center = CGPointMake(self.gameOverLabel.center.x+500, self.gameOverLabel.center.y)
                    })
                    break;
                case 2:
                    lockAllButtons()
                    // mainLabel.text = "X's WIN!!"
                    gameOverLabel.text = "X's Win!!"
                    gameOverLabel.hidden = false
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.gameOverLabel.center = CGPointMake(self.gameOverLabel.center.x+500, self.gameOverLabel.center.y)
                    })
                    break;
                default:
                    toggleWhosTurn()
                    break;
            }
        }
    }
    
    // Player 1 == O's, Player 2 == X's
    func setBoard(index: Int, player: Int) {
        let row = (index - 1) / 3
        let col = (index - 1) % 3
        theBoard[row][col] = player
        print(theBoard[0])
        print(theBoard[1])
        print(theBoard[2])
        print(" ")
    }
    
    func toggleWhosTurn() {
        naughtsTurn = !naughtsTurn
        if naughtsTurn {
            whosnextButton.setImage(UIImage(named: "o.png"), forState: .Normal)
        } else {
            whosnextButton.setImage(UIImage(named: "x.png"), forState: .Normal)
        }
    }
    
    // examine the board, return 1 if O's win, return 2 if X's win
    func getWinner() -> Int {
        
        for player in 1...2 {
            for row in 0...2 {
                if playerWinsRow(player,row: row) {
                    return player
                }
                if playerWinsCol(player,col: row) {
                    return player
                }
            }
            if  playerWinsDiag(player) {
                return player
            }
        }
        
        return 0
    }
    
    func playerWinsDiag(player: Int) -> Bool {
        if theBoard[0][0] == player && theBoard[1][1] == player && theBoard[2][2] == player {
            return true
        }
        if theBoard[0][2] == player && theBoard[1][1] == player && theBoard[2][0] == player {
            return true
        }
        return false
    }
    
    func playerWinsRow(player: Int, row: Int) -> Bool {
        for col in 0...2 {
            if( theBoard[row][col] != player) {
                return false
            }
        }
        return true
    }
    
    func playerWinsCol(player: Int, col: Int) -> Bool {
        for row in 0...2 {
            if( theBoard[row][col] != player) {
                return false
            }
        }
        return true
    }
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
